
Shader "Custom/Draw" {
	Properties{
		_Color("Color", Color) = (0.5,0.5,0.5,1)
		_MainTex("MainTex", 2D) = "white" {}
	}
		SubShader{
			Tags {
				"Queue" = "AlphaTest"
				"RenderType" = "TransparentCutout"
			}
			LOD 200
			Pass {
				Name "FORWARD"
				Tags {
					"LightMode" = "Always"
				}


				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"
				#pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
				#pragma target 3.0
				uniform float4 _Color;
				uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
				struct VertexInput {
					float4 vertex : POSITION;
					float2 texcoord0 : TEXCOORD0;
				};
				struct VertexOutput {
					float4 pos : SV_POSITION;
					float2 uv0 : TEXCOORD0;
				};
				VertexOutput vert(VertexInput v) {
					VertexOutput o = (VertexOutput)0;
					o.uv0 = v.texcoord0;
					o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
					return o;
				}
				float4 frag(VertexOutput i) : COLOR {
					float4 tex = step(0.1,tex2D(_MainTex, i.uv0))*_Color;
					clip(tex.a - 0.5);
					return tex;
				}
								ENDCG
							}
	}
		FallBack "Diffuse"
}
