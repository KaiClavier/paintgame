﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{

    public static Player instance;

    public bool isPaused;

    public PhysicMaterial playerPhysicMaterial;

    public float walkSpeed;
    public float runSpeed;
    public float walkDrag;
    public float runDrag;
    public float gravity;
    float drag;

    Transform playerHeadTransform;
    public Transform playerCameraTransform;
    Camera playerCamera;
    CapsuleCollider myCapsuleCollider;
    Rigidbody myRigidbody;

    public float jumpForce;
    public float jumpTimeLeeway;

    float jumpRequestedTime;
    float leftGroundTime;
    bool jumpRequested;
    bool jumped;

    public LayerMask groundedMask;

    float groundCheckRadius;
    float groundCheckDistance;
    Ray groundCheckRay;
    bool isGrounded;

    Vector3 standPos;
    Vector3 duckPos;
    Vector3 headTargetPos;

    public float duckSmooth;
    public float duckHeight;

    public Vector2 mouseSensitivity;
    public float mouseLookLimit;

    public bool invertMouse;

    Vector2 mouseInput;
    Vector2 smoothedMouseInput;
    public float mouseSmoothing;

    Vector2 moveInput;
    float moveForce;

    Vector3 currentMoveForce;
    Vector3 currentDragForce;
    Vector3 lookRotation;

    void Awake()
    {
        instance = this;
    }

    public void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();
        playerCamera = GetComponentInChildren<Camera>();
        playerCameraTransform = playerCamera.transform;
        playerHeadTransform = playerCameraTransform.parent;
        myCapsuleCollider = GetComponentInChildren<CapsuleCollider>();
        groundCheckRadius = myCapsuleCollider.radius * 0.95f;
        groundCheckDistance = (playerHeadTransform.localPosition.y - groundCheckRadius) + 0.1f;
        groundCheckRay.direction = Vector3.down;
        standPos = playerHeadTransform.localPosition;
        duckPos = standPos;
        duckPos.y = duckHeight;
        headTargetPos = standPos;
    }

    void Update()
    {
        if (!isPaused)
        {
            CheckGrounded();
            DoLook();

            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (!jumped && isGrounded)
                {
                    DoJump();
                }
                else
                {
                    if (!jumped && Time.time - leftGroundTime < jumpTimeLeeway)
                    {
                        DoJump();
                    }
                    else
                    {
                        jumpRequested = true;
                        jumpRequestedTime = Time.time;
                    }
                }
            }

            if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.C))
            {
                headTargetPos = duckPos;
            }
            else
            {
                headTargetPos = standPos;
            }

            if (Input.GetKey(KeyCode.LeftShift))
            {
                moveForce = runSpeed;
                drag = runDrag;
            }
            else
            {
                moveForce = walkSpeed;
                drag = walkDrag;
            }

            playerHeadTransform.localPosition = Vector3.Lerp(playerHeadTransform.localPosition, headTargetPos, duckSmooth);

        }
    }

    void FixedUpdate()
    {
        if (!isPaused)
        {

            moveInput.x = Input.GetAxisRaw("Horizontal");
            moveInput.y = Input.GetAxisRaw("Vertical");

            moveInput = Vector2.ClampMagnitude(moveInput, 1);

            currentDragForce.x = -myRigidbody.velocity.x;
            currentDragForce.z = -myRigidbody.velocity.z;

            currentMoveForce = (playerHeadTransform.forward * moveInput.y) + (playerHeadTransform.right * moveInput.x) + (currentDragForce * drag) + (Vector3.down * gravity);

            myRigidbody.AddForce(currentMoveForce * moveForce);

            if (isGrounded && moveInput.magnitude < 0.1f && myRigidbody.velocity.magnitude < 1f)
            {
                playerPhysicMaterial.staticFriction = 25;
                playerPhysicMaterial.dynamicFriction = 25;
            }
            else
            {
                playerPhysicMaterial.staticFriction = 1;
                playerPhysicMaterial.dynamicFriction = 1;
            }
        }
    }


    void DoJump()
    {
        jumped = true;
        jumpRequested = false;
        jumpRequestedTime = 0;
        myRigidbody.velocity = Vector3.Scale(myRigidbody.velocity, (Vector3.one - Vector3.up));

        myRigidbody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    }

    void CheckGrounded()
    {

        groundCheckRay.origin = playerHeadTransform.position;
        if (Physics.SphereCast(groundCheckRay, groundCheckRadius, groundCheckDistance, groundedMask))
        {
            if (!isGrounded)
            {
                jumped = false;
                if (jumpRequested && Time.time - jumpRequestedTime < jumpTimeLeeway)
                {
                    DoJump();
                }
            }
            isGrounded = true;
        }
        else
        {
            leftGroundTime = Time.time;
            isGrounded = false;
        }
    }

    void DoLook()
    {
        mouseInput.x = Input.GetAxisRaw("Mouse X");
        if (!invertMouse)
            mouseInput.y = -Input.GetAxisRaw("Mouse Y");
        else
            mouseInput.y = Input.GetAxisRaw("Mouse Y");

        mouseInput = Vector2.Scale(mouseInput, mouseSensitivity);

        smoothedMouseInput = Vector2.Lerp(mouseInput, smoothedMouseInput, mouseSmoothing);

        playerHeadTransform.Rotate(0, smoothedMouseInput.x, 0);

        lookRotation.x = Mathf.Clamp(lookRotation.x + smoothedMouseInput.y, -mouseLookLimit, mouseLookLimit);
        playerCameraTransform.localEulerAngles = lookRotation;
    }

    public void Pause()
    {
        isPaused = true;
    }

    public void UnPause()
    {
        isPaused = false;
    }

    public void EnableCamera(bool b)
    {
        playerCamera.enabled = b;
    }

    public void UpdateFOV(float fov)
    {
        playerCamera.fieldOfView = fov;
    }

    public void UpdateSensitivity(float sens)
    {
        mouseSensitivity.x = sens;
        mouseSensitivity.y = sens;
    }

    public void UpdateInvert(bool i)
    {
        invertMouse = i;
    }

}
