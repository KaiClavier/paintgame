﻿using UnityEngine;
using System.Collections;

public class CanvasSetup : MonoBehaviour {
	public int maxTexSize = 1024; //widest size of texture will be this
    public enum Filter {
        Point = FilterMode.Point,
        Bilinear = FilterMode.Bilinear,
        Trilinear = FilterMode.Trilinear
    }
    public Filter _filter = Filter.Point;
    public enum AntiAliasing{
        Off = 1,
        TwoPass = 2,
        FourPass = 4,
        EightPass = 8
    }
    public AntiAliasing _antiAliasing = AntiAliasing.Off;
	// Use this for initialization
	void Start () {
        Camera myCam = GetComponentInChildren<Camera>();
        
        Vector3 canvasScale = transform.localScale;
        Vector2 texSize = new Vector2(maxTexSize,maxTexSize);
        //find out rendertexture size
	    if(canvasScale.x > canvasScale.y){ //image is wide!
	        texSize.y *= canvasScale.y / canvasScale.x;
	    }else{ //image is tall or square
	        texSize.x *= canvasScale.x / canvasScale.y;
	    }
		//width, height, depth, color format, readwrite
        RenderTexture myRenTex = new RenderTexture((int)texSize.x,(int)texSize.y,0);
        myRenTex.filterMode = (FilterMode)_filter;
        myRenTex.antiAliasing = (int)_antiAliasing; //must be 1, 2, 4, or 8. Amount of passes.
        myRenTex.name = "CanvasRenderTex";
        myRenTex.Create();

        //resize camera
        myCam.orthographicSize = canvasScale.y / 2;

        //apply new rendertexture
        GetComponent<Renderer>().material.mainTexture = myRenTex;
        myCam.targetTexture = myRenTex;

        //reset canvas
        myCam.clearFlags = CameraClearFlags.Color;
        myCam.backgroundColor = Color.white;
        myCam.Render();
        myCam.clearFlags = CameraClearFlags.Nothing;
    }
}
