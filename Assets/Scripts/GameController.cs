﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour
{

    public static GameController instance;

    public bool screenDraw = false;
    public bool canPause = true;
    public bool lockCursor = true;
    public float drawSize = 0.5f;
    public float opacity = 1.0f; //changes opacity of stroke layer, not brush
    public int smoothing = 0;
    public int activeBrush = 0;
    private List<Transform> brushes = new List<Transform>();
    private List<Transform> trails = new List<Transform>();
    public LayerMask strokeMask;

    public Transform draw;
    private Vector3 lastPosition; //for smoothing
    private FancyCanvas currentCanvas; //the last canvas to be interacted with
    Color drawColor;
    //public RenderTexture renTex;

    RaycastHit rayHit;

    void Awake(){
        instance = this;
    }

    void Start(){
        lastPosition = draw.position;

        foreach(Transform child in draw){ //gather brushes
            brushes.Add(child);
        }
        NewSmooth(smoothing);
        LockCursor(true); //start in fps mode
        
    }
    void LockCursor(bool maybe){
        lockCursor = maybe;
        //lock cursor
        if(lockCursor == true){
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }else{
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    void NewSmooth(int thisMany) {
        smoothing = thisMany;
        //clear out old trails
        trails.Clear();
        foreach(Transform child in brushes[activeBrush].Find("Draw")){
            Destroy(child.gameObject);
        }
        //copy the trail-free brush
        Transform myRef = Instantiate(brushes[activeBrush].Find("Draw"));
        for(int i=0,iL=smoothing;i<iL;i++){
            Transform newSmooth = Instantiate(myRef);
            newSmooth.parent = brushes[activeBrush].Find("Draw");
            trails.Add(newSmooth);
        }
        Destroy(myRef.gameObject);
    }

    void Smooth() {
        for(int i=0,iL=trails.Count;i<iL;i++){
            trails[i].position = Vector3.Lerp(draw.position,lastPosition,(i+1.0f)/(iL+1.0f));
            //Debug.Log("New Pos: " + draw.position + " Old pos: " + lastPosition + " This pos: " + trails[i].position + " should be at " + 
                //(i+1) + "/" + (iL+1) + "=" + (i+1.0f)/(iL+1.0f));
        }
    }

    void Update(){
        //show preview
        /*
        Vector3 mousePos = new Vector3(Input.mousePosition.x,Input.mousePosition.y,Player.instance.playerCameraTransform.forward.z * 0.1f);
        mousePos = Player.instance.GetComponentInChildren<Camera>().ScreenToWorldPoint(mousePos);
        //mousePos.y += Camera.main.transform.position.y;
        */
        Ray screenRay = Player.instance.GetComponentInChildren<Camera>().ScreenPointToRay(Input.mousePosition);
        if(screenDraw == false){
            Debug.DrawRay(Player.instance.playerCameraTransform.position, Player.instance.playerCameraTransform.forward * 50, Color.red, 0.0f, true);
        }else{
            //Debug.DrawRay(mousePos, Player.instance.playerCameraTransform.forward * 50, Color.red, 0.0f, true);
        }

        if ((screenDraw == false && Physics.Raycast(Player.instance.playerCameraTransform.position, Player.instance.playerCameraTransform.forward, out rayHit, 50, strokeMask)) ||
            (screenDraw == true && Physics.Raycast(screenRay, out rayHit, 50, strokeMask))){
            lastPosition = draw.position; //save last position
            draw.position = rayHit.point;
            draw.rotation = rayHit.transform.rotation;
            draw.localScale = Vector3.one * drawSize;
            draw.gameObject.SetActive(true);

            currentCanvas = rayHit.transform.parent.GetComponent<FancyCanvas>();

            if (Input.GetMouseButtonDown(0)){
                brushes[activeBrush].Find("Draw").gameObject.SetActive(true);
                
            }
            if (Input.GetMouseButton(0)){ //if player is trying to draw...
                Smooth();
                UpdateOpacity();
                //will hit the stroke canvas
                currentCanvas.RenderStroke();
            }else{
                brushes[activeBrush].Find("Draw").gameObject.SetActive(false);
            }
            if(Input.GetMouseButtonUp(0)){
                currentCanvas.RenderCanvas(); //apply
            }
        }else{
            //draw.position = Vector3.one * -999;
            draw.gameObject.SetActive(false);
        }
        /* manual rendering
        if(Input.GetKeyDown("m")){
            currentCanvas.RenderCanvas();
            //Debug.Log("Rendering!");
        }
        */

        if (Input.GetMouseButtonDown(1)){
            drawColor.r = Random.value;
            drawColor.g = Random.value;
            drawColor.b = Random.value;
            drawColor.a = 1;
            SpriteRenderer[] mySprites = brushes[activeBrush].GetComponentsInChildren<SpriteRenderer>(true);
            for(int i=0,iL=mySprites.Length;i<iL;i++){
                mySprites[i].color = drawColor;
            }
        }

        if(Input.GetKeyDown("[")){
            drawSize -= 0.1f;
        }
        if(Input.GetKeyDown("]")){
            drawSize += 0.1f;
        }

        if(Input.GetKeyDown("-")){
            opacity -= 0.1f;
            UpdateOpacity();
        }
        if(Input.GetKeyDown("=")){
            opacity += 0.1f;
            UpdateOpacity();
        }
        if(Input.GetKeyDown("e")){ //toggle screendraw mode
            screenDraw = !screenDraw;
            Player.instance.enabled = !screenDraw;
            LockCursor(!screenDraw);
        }

        if(Input.GetKeyDown("`")){
            if(currentCanvas != null){
                currentCanvas.SaveToDesktop();
            }
        }

    }
    void UpdateOpacity(){
        if(currentCanvas != null){
            currentCanvas.SetStrokeOpacity(opacity);
            Color tempColor = drawColor;
            tempColor.a = opacity;
            brushes[activeBrush].Find("Reticle").GetComponent<SpriteRenderer>().color = tempColor;
        }
    }
}
