﻿using UnityEngine;
using System.Collections;
using System; //for date

public class Clavian {
	public static bool CoinFlip () {
		return UnityEngine.Random.value < 0.5f;
	}
	public static int Wrap (int x, int min, int max){
		int myMin = min;
		int myMax = max;
		if(min > max){ //then flip!
			myMin = max;
			myMax = min;
		}
		int diff = myMax - myMin;
		while (x < myMin) {x += diff;}
		while (x > myMax) {x -= diff;}
		return x;
	}
	public static float AddOne (float woah) { //takes away one if negative
		if(woah > 0){
			//Debug.Log("Higher!");
			return woah + 1;
		}else if(woah < 0){
			//Debug.Log("Lower!!");
			return woah - 1;
		}else{
			return woah;
		}
	}
	/*
	public static string DesktopFolder () {
		return Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
	}
	public static string DateString () {
		DateTime myDate = DateTime.Now;
        int[] dates = new int[] {
            myDate.Year,myDate.Month,myDate.Day,myDate.Hour,myDate.Minute,myDate.Second
        };
        string meridian;
        if(dates[3] == 24){ //midnight
            dates[3] = 12;
            meridian = "AM";
        }else if(dates[3] > 12){
            dates[3] = (dates[3] - 12);
            meridian = "PM";
        }else if(dates[3] == 12){ //noon
            meridian = "PM";
        }else{
            meridian = "AM";
        }
        string[] datesStr = new string[] {
            dates[0].ToString(),dates[1].ToString(),dates[2].ToString(),dates[3].ToString(),dates[4].ToString(),dates[5].ToString()
        };
        for(int i=1,iL=datesStr.Length;i<iL;i++){ //start at 1, not 0
            if(datesStr[i].Length == 1){
                datesStr[i] = "0" + datesStr[i];
            }
        }
        return datesStr[0] +"-"+ datesStr[1] +"-"+ datesStr[2] +" at "+ datesStr[3] +":"+ datesStr[4] +":"+ datesStr[5] +" "+ meridian;
	}
	*/
}
