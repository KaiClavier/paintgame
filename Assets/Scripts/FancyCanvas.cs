﻿using UnityEngine;
using System.Collections;
using System; //for getting date & folderpath?

public class FancyCanvas : MonoBehaviour {
	public int maxTexSize = 1024; //widest size of texture will be this
    public enum Filter {
        Point = FilterMode.Point,
        Bilinear = FilterMode.Bilinear,
        Trilinear = FilterMode.Trilinear
    }
    public Filter _filter = Filter.Point;
    public enum AntiAliasing{
        Off = 1,
        TwoPass = 2,
        FourPass = 4,
        EightPass = 8
    }
    public AntiAliasing _antiAliasing = AntiAliasing.Off;

    private Camera canvasCam;
    private Camera strokeCam;
    private RenderTexture canvasTex;
    private Transform canvasMesh;
    private Transform strokeMesh;
    private RenderTexture strokeTex;

    private Vector3 canvasScale;
    private Vector2 texSize;
	// Use this for initialization
	void Start () {
        canvasCam = transform.Find("CanvasCam").GetComponent<Camera>();
        strokeCam = transform.Find("StrokeCam").GetComponent<Camera>();
        canvasMesh = transform.Find("MainCanvas");
        strokeMesh = transform.Find("StrokeCanvas");

        CalculateCanvasSize();

        NewMainCanvas();
        NewStrokeCanvas();
    }
    void CalculateCanvasSize () {
        //(8/24)*1024 = 341.3...
        //(8/24)*1023 = 341!
        canvasScale = transform.localScale;
        texSize = new Vector2(maxTexSize,maxTexSize);
        //find out rendertexture size!
        if(canvasScale.x > canvasScale.y){ //image is wide!
            //texSize.y = (canvasScale.y / canvasScale.x) * texSize.x;
            
            for(int i=0,iL=20;i<iL;i++){//try 20 times to fix...
                texSize.y = (canvasScale.y / canvasScale.x) * texSize.x;
                if(texSize.y == (int)texSize.y){
                    break; //stop looping!
                }
                texSize.x--; //decrease by 1
            }
            
        }else if(canvasScale.x < canvasScale.y){ //image is tall
            //texSize.x = (canvasScale.x / canvasScale.y) * texSize.y;
            //this STILL leaves some kinda proplem
            for(int i=0,iL=20;i<iL;i++){//try 20 times to fix...
                texSize.x = (canvasScale.x / canvasScale.y) * texSize.y;
                if(texSize.x == (int)texSize.x){
                    break; //stop looping!
                }
                texSize.y--; //decrease by 1
            }
            
        }
    }
    void NewMainCanvas () {
        //width, height, depth, color format, readwrite
        canvasTex = new RenderTexture((int)texSize.x,(int)texSize.y,0);
        canvasTex.filterMode = FilterMode.Point; //always going to copy whatever stroke is
        canvasTex.antiAliasing = 1; //same to this
        canvasTex.name = "CanvasRenderTex";
        canvasTex.Create();
        //resize camera
        canvasCam.orthographicSize = canvasScale.y / 2;
        //apply new rendertexture
        canvasMesh.GetComponent<Renderer>().material.mainTexture = canvasTex;
        canvasCam.targetTexture = canvasTex;
        //reset canvas
        canvasCam.clearFlags = CameraClearFlags.Color;
        canvasCam.backgroundColor = Color.white;
        canvasCam.Render();
        canvasCam.clearFlags = CameraClearFlags.Nothing;
    }
    void NewStrokeCanvas (){
        //width, height, depth, color format, readwrite
        strokeTex = new RenderTexture((int)texSize.x,(int)texSize.y,0);
        strokeTex.filterMode = (FilterMode)_filter;
        strokeTex.antiAliasing = (int)_antiAliasing; //must be 1, 2, 4, or 8. Amount of passes.
        strokeTex.name = "StrokeRenderTex";
        strokeTex.Create();
        //resize camera
        strokeCam.orthographicSize = canvasScale.y / 2;
        //apply new rendertexture
        strokeMesh.GetComponent<Renderer>().material.mainTexture = strokeTex;
        strokeCam.targetTexture = strokeTex;
        //reset canvas
        strokeCam.clearFlags = CameraClearFlags.Color;
        strokeCam.backgroundColor = Color.clear;
        strokeCam.Render();
        strokeCam.clearFlags = CameraClearFlags.Nothing;
    }  
    public void RenderCanvas () {
        canvasCam.Render();
        //Graphics.Blit(strokeTex,canvasTex,canvasMesh.GetComponent<Renderer>().material,-1);
        //clear stroke cam
        NewStrokeCanvas();
    }
    public void RenderStroke () {
        strokeCam.Render();
    }
    public void SetStrokeOpacity (float what){
        Color myOpacity = new Color(1,1,1,what);
        strokeMesh.GetComponent<Renderer>().material.color = myOpacity;
    }
    public void SaveToDesktop () {
        //StartCoroutine(SaveIt());
        Texture2D flatTex = GetRTPixels(canvasTex);
        byte[] bytes = flatTex.EncodeToPNG();
        Destroy(flatTex);

        string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        string date = System.DateTime.Now.ToString("yyyy-MM-dd @ HH.mm.ss tt");
        //Debug.Log(folderPath + " " + date);
        //actually save png to file
        int currentShot = 0;
        string filePath = "";
        string organizeFolder = "Art Exports";
        //create organizational folder if it doesn't exist yet
        //apparently this is done automatically?
        System.IO.Directory.CreateDirectory(folderPath + "/" + organizeFolder);
        do{
            filePath = folderPath + "/" + organizeFolder + "/Art Export " + date + " " + currentShot + ".png";
        }while(System.IO.File.Exists(filePath));
        System.IO.File.WriteAllBytes(filePath, bytes);
        Debug.Log("Art saved to " + filePath);
    }
    Texture2D GetRTPixels(RenderTexture rt) { //get rendertexture pixels
        // Remember currently active render texture
        RenderTexture currentActiveRT = RenderTexture.active;
        // Set the supplied RenderTexture as the active one
        RenderTexture.active = rt;
        // Create a new Texture2D and read the RenderTexture image into it
        Texture2D tex = new Texture2D(rt.width, rt.height);
        tex.ReadPixels(new Rect(0, 0, tex.width, tex.height), 0, 0);
        // Restore previously active render texture
        RenderTexture.active = currentActiveRT;
        return tex;
    }
}
